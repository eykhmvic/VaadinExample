myForm
==============

Template for a simple Vaadin application that only requires a Servlet 3.0 container to run.


Workflow
========

To compile the entire project, run "mvn compile".

To run the application, run "mvn jetty:run" and open http://localhost:8080/ .

Test
========
run "mvn verify" (works only on my computer with my browser driver. Go to class PersonFormIT and change driver)