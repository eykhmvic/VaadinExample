import com.vaadin.testbench.elements.*;
import myVaadin.model.PersonStatus;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.vaadin.testbench.TestBenchTestCase;

import java.awt.*;

public class PersonFormIT extends TestBenchTestCase {
    @Before
    public void setup() throws Exception {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\VEykhmann\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        setDriver(driver);
//        getDriver().get("http://localhost:8080/");
    }


    @After
    public void tearDown() throws Exception {
        getDriver().quit();
    }

    @Test
    public void testAddressBook() {
        getDriver().get("http://localhost:8080/");
        Assert.assertTrue($(GridElement.class).exists());
    }

    @Test
    public void tryToAddPerson() {
        getDriver().get("http://localhost:8080/");
        $(TextFieldElement.class).caption("First name").first().setValue("Bill");;
        $(TextFieldElement.class).caption("Last name").first().setValue("Milligan");
        $(TextFieldElement.class).caption("Email").first().setValue("test@mail.com");

        $(DateFieldElement.class).caption("Birthday").first().setValue("3/13/18");
        $(TextFieldElement.class).caption("Phone number").first().setValue("123456789");
        $(NativeSelectElement.class).caption("Status").first().setValue(PersonStatus.state1.toString());
        $(RadioButtonGroupElement.class).caption("Sex").first().setValue("Male");
//        org.vaadin.teemu.switchui.SwitchElement areyoudummyorgvaadinteemuswitchuiSwitch = $(org.vaadin.teemu.switchui.SwitchElement.class).caption("Are you dummy? :)").first();
        $(RadioButtonGroupElement.class).caption("Sex").first().setValue("Male");
        boolean enabled = $(ButtonElement.class).caption("Save").first().isEnabled();
        Assert.assertTrue(enabled);

    }


}