package myVaadin.view;

import myVaadin.model.Person;
import myVaadin.presenter.Presenter;

import java.util.List;

public interface PersonView {
    public void setPerson(Person person);

    public void displayPersons(List<Person> persons);

    public Presenter getPresenter();

    public void setPresenter(Presenter presenter);

    public void setDeleteButtonEnabled(boolean enabled);

    public void updateSaveButton();
}
