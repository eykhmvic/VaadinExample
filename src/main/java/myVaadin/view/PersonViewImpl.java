package myVaadin.view;

import com.vaadin.event.ShortcutAction;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import lombok.Getter;
import lombok.Setter;
import myVaadin.model.Person;
import myVaadin.presenter.Presenter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

@SpringComponent
@UIScope
public class PersonViewImpl extends CustomComponent implements PersonView {
    @Setter
    @Getter
    Presenter presenter;
    private Button clear = new Button("Clear");
    private Button save = new Button("Save");
    private Button newPerson = new Button("Add new Person");
    private Button deletePerson = new Button("Delete");
    @Getter
    PersonFormImpl personForm;
    private Grid<Person> grid = new Grid<>(Person.class);

    @Autowired
    public void setPersonForm(PersonFormImpl personForm) {
        this.personForm = personForm;
    }

    @PostConstruct
    public void init() {
        personForm.setPersonView(this);
        createView();
        addListeners();
    }

    public PersonViewImpl() {
    }

    private void createView() {
        VerticalLayout personFormLayout = new VerticalLayout(personForm, new HorizontalLayout(save, clear, deletePerson));
        HorizontalLayout main = new HorizontalLayout(grid, personFormLayout);
        personFormLayout.setWidthUndefined();
        main.setSizeFull();
        grid.setSizeFull();
        main.setExpandRatio(grid, 1);
        VerticalLayout verticalLayout = new VerticalLayout(newPerson, main);

        setCompositionRoot(verticalLayout);
        addLogicToButtons();
        updateSaveButton();
    }

    private void addLogicToButtons() {
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        deletePerson.setStyleName(ValoTheme.BUTTON_DANGER);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        setDeleteButtonEnabled(false);
    }

    private void addListeners() {
        clear.addClickListener(a -> {
            personForm.clearAll();
            updateSaveButton();
        });
        save.addClickListener(clickEvent -> {
            presenter.saveButtonClicked(personForm.getCurrentPerson());
            setDeleteButtonEnabled(false);
        });
        deletePerson.addClickListener(clickEvent -> {
            presenter.deleteButtonClicked(personForm.getCurrentPerson());
            setDeleteButtonEnabled(false);
        });
        newPerson.addClickListener(o -> {
            presenter.addNewPersonButtonClicked();
            setDeleteButtonEnabled(false);
        });
        grid.asSingleSelect().addValueChangeListener(valueChangeEvent -> {
            personForm.setPerson(valueChangeEvent.getValue());
            setDeleteButtonEnabled(true);
        });
    }


    public void updateSaveButton() {
        save.setEnabled(personForm.isValidFrom());
    }

    public void setDeleteButtonEnabled(boolean enabled) {
        deletePerson.setEnabled(enabled);
    }

    public void displayPersons(List<Person> persons) {
        grid.setItems(persons);
    }

    public void setPerson(Person person) {
        personForm.setPerson(person);
        updateSaveButton();
    }


}
