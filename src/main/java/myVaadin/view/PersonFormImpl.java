package myVaadin.view;

import com.vaadin.data.*;
import com.vaadin.server.Setter;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import lombok.Data;
import lombok.Getter;
import myVaadin.model.Person;
import myVaadin.model.PersonStatus;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.vaadin.teemu.switchui.Switch;
import com.vaadin.data.validator.EmailValidator;
@SpringComponent
@UIScope
public class PersonFormImpl extends FormLayout implements PersonForm {
    private TextField firstName = new TextField("First name");
    private TextField lastName = new TextField("Last name");
    private TextField email = new TextField("Email");
    private TextField phone = new TextField("Phone number");
    private DateField birthday = new DateField("Birthday");
    private NativeSelect<PersonStatus> statusNativeSelect = new NativeSelect<>("Status");
    private Switch dummy_switch = new Switch("Are you dummy? :)");
    private RadioButtonGroup<String> sex = new RadioButtonGroup<>("Sex");


    Binder<Person> binder = new Binder<>(Person.class);
    @Getter
    PersonView personView;


    @Override
    public void setPersonView(PersonView personView) {
        this.personView = personView;
    }

    public PersonFormImpl() {
        setNecessaryAttributesForFields();
        bind();
        setView();
    }

    private void setView() {
        setSizeUndefined();
        addComponents(firstName, lastName, email, birthday, phone, statusNativeSelect, dummy_switch, sex);
    }

    private void setNecessaryAttributesForFields() {
        sex.setItems("Male", "Female");
        statusNativeSelect.setItems(PersonStatus.values());
        phone.setRequiredIndicatorVisible(true);
        firstName.setRequiredIndicatorVisible(true);
        lastName.setRequiredIndicatorVisible(true);
        email.setRequiredIndicatorVisible(true);
        birthday.setRequiredIndicatorVisible(true);
        email.setRequiredIndicatorVisible(true);
        statusNativeSelect.setRequiredIndicatorVisible(true);
        statusNativeSelect.setEmptySelectionAllowed(false);
    }

    private void bind() {
        binder.forField(statusNativeSelect).asRequired("Required!").bind(Person::getStatus, Person::setStatus);
        binder.bind(dummy_switch, Person::isDummy, Person::setDummy);
        binder.forField(sex).asRequired("Required!").bind(new ValueProvider<Person, String>() {
            @Override
            public String apply(Person person) {
                return person.getSex().name();
            }
        }, new Setter<Person, String>() {
            @Override
            public void accept(Person person, String s) {
                if (EnumUtils.isValidEnum(Person.sex.class, s)) {
                    person.setSex(Person.sex.valueOf(s));
                } else {
                    person.setSex(Person.sex.Male);
                }
            }
        });
        binder.forField(phone).withValidator(StringUtils::isNumeric, "Numbers only").asRequired("Required").withValidator(o -> o.length() == 9, "9 numbers").bind(Person::getPhone, Person::setPhone);
        binder.forField(email).withValidator(new EmailValidator("enter valid email")).bind(Person::getEmail, Person::setEmail);
        binder.forField(firstName).asRequired("Required!").bind(Person::getFirstName, Person::setFirstName);
        binder.forField(lastName).asRequired("Required!").bind(Person::getLastName, Person::setLastName);
        binder.forField(birthday).asRequired("Required!").bind(Person::getBirthday, Person::setBirthday);
        binder.bindInstanceFields(this);
        binder.addValueChangeListener(valueChangeEvent -> personView.updateSaveButton());
    }

    public void clearAll() {
        binder.getFields().forEach(HasValue::clear);
    }

    public void setPerson(Person person) {
        binder.setBean(person);
    }

    public Person getCurrentPerson() {
        return binder.getBean();
    }

    public boolean isValidFrom() {
        return binder.isValid();
    }
}
