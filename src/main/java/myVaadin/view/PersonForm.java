package myVaadin.view;

import myVaadin.model.Person;

public interface PersonForm {
    public boolean isValidFrom();
    public Person getCurrentPerson();
    public void setPerson(Person person);
    public void setPersonView(PersonView personView);
    public PersonView getPersonView();
}
