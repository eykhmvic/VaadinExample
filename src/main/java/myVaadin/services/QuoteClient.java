package myVaadin.services;

import myVaadin.model.wsdl.GetCitiesByCountry;
import myVaadin.model.wsdl.GetCitiesByCountryResponse;
import myVaadin.model.wsdl.GetWeather;
import myVaadin.model.wsdl.GetWeatherResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class QuoteClient extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(QuoteClient.class);

    public GetCitiesByCountryResponse getCities(String ticker) {

//        GetWeather request = new GetWeather();
        GetCitiesByCountry request = new GetCitiesByCountry();
        request.setCountryName(ticker);

//        request.setCityName(ticker);
//        request.setCountryName("Russian Federation");
//        log.info("Requesting quote for "+ticker);
        GetCitiesByCountryResponse response = (GetCitiesByCountryResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://www.webservicex.com/globalweather.asmx",
                        request,
                        new SoapActionCallback("http://www.webserviceX.NET/GetCitiesByCountry"));

        return response;
    }

}