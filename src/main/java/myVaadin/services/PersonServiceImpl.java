package myVaadin.services;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import myVaadin.model.Person;
import myVaadin.model.PersonBuilder;
import myVaadin.model.PersonStatus;
import org.springframework.context.annotation.Scope;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SpringComponent
@UIScope
public class PersonServiceImpl implements PersonService {
//    private static PersonServiceImpl instance;
    private final HashMap<Long, Person> persons = new HashMap<>();
    private long nextId = 0;

    public PersonServiceImpl() {
    }


    public Person getNewPerson() {
        return new PersonBuilder().sex(Person.sex.Male).status(PersonStatus.state1).build();
    }

//    public static PersonServiceImpl getInstance() {
//        if (instance == null) {
//            instance = new PersonServiceImpl();
//        }
//        return instance;
//    }

    public synchronized void delete(Person person) {
        persons.remove(person.getId());
    }

    public synchronized void save(Person person) {
        if (person == null) {
            System.err.println("Try to save null person");
            return;
        }
        if (person.getId() == null) {
            person.setId(nextId++);
        }
        persons.put(person.getId(), person);
    }

    public synchronized List<Person> getAll() {
        return new ArrayList<>(persons.values());
    }
}
