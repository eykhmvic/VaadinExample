package myVaadin.services;

import myVaadin.model.Person;

import java.util.List;

public interface PersonService {
    public void delete(Person person);

    public void save(Person person);

    public List<Person> getAll();

    public Person getNewPerson();

}
