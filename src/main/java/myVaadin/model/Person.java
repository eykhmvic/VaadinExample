package myVaadin.model;

import lombok.Data;

import java.time.LocalDate;

import static myVaadin.model.Person.sex.*;

@Data
public class Person {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private LocalDate birthday;
    private PersonStatus status;
    private sex sex;
    private boolean dummy;

    public enum sex {
        Male, Female
    }

    Person() {
    }

    @Override
    public Person clone() throws CloneNotSupportedException {
        return (Person) super.clone();
    }
}
