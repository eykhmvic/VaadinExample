package myVaadin.model;

import java.time.LocalDate;

public class PersonBuilder {
    private Person person;
    private long nextId = 0;

    public PersonBuilder() {
        person = new Person();
    }

    public PersonBuilder firstName(String name) {
        person.setFirstName(name);
        return this;
    }

    public PersonBuilder lastName(String name) {
        person.setLastName(name);
        return this;
    }

    public PersonBuilder email(String email) {
        person.setEmail(email);
        return this;
    }

    public PersonBuilder phone(String name) {
        person.setPhone(name);
        return this;
    }

    public PersonBuilder birthday(LocalDate name) {
        person.setBirthday(name);
        return this;
    }

    public PersonBuilder dummy(boolean name) {
        person.setDummy(name);
        return this;
    }

    public PersonBuilder sex(Person.sex sex) {
        person.setSex(sex);
        return this;
    }

    public PersonBuilder status(PersonStatus name) {
        person.setStatus(name);
        return this;
    }

    public PersonBuilder id(Long id) {
        person.setId(id);
        return this;
    }

    public Person build() {
        return person;
    }

}
