package myVaadin.presenter;

import myVaadin.model.Person;
import myVaadin.services.PersonService;
import myVaadin.view.PersonView;
import myVaadin.view.PersonViewImpl;

public interface Presenter {
    public PersonService getPersonService();

    public PersonViewImpl getPersonViewImpl();

    public void saveButtonClicked(Person person);

    public void deleteButtonClicked(Person currentPerson);

    public void addNewPersonButtonClicked();
}
