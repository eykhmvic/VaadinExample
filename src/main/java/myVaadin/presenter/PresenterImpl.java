package myVaadin.presenter;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import lombok.Data;
import myVaadin.model.Person;
import myVaadin.services.PersonService;
import myVaadin.view.PersonView;
import myVaadin.view.PersonViewImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Data
@SpringComponent
@UIScope
public class PresenterImpl implements Presenter {
    PersonService personService;
    PersonViewImpl personViewImpl;

    @Autowired
    public PresenterImpl(PersonViewImpl personView, PersonService personService) {
        this.personViewImpl = personView;
        this.personService = personService;
        personView.setPerson(personService.getNewPerson());
        personView.setPresenter(this);
    }

    public void saveButtonClicked(Person person) {
        personService.save(person);
        personViewImpl.setPerson(personService.getNewPerson());
        personViewImpl.displayPersons(personService.getAll());
    }

    public void deleteButtonClicked(Person currentPerson) {
        personService.delete(currentPerson);
        personViewImpl.displayPersons(personService.getAll());
        personViewImpl.setPerson(personService.getNewPerson());
    }

    public void addNewPersonButtonClicked() {
        personViewImpl.setPerson(personService.getNewPerson());
    }
}
